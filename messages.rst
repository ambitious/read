.. messages.rst -- travelling between watch and application
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


.. _protocol-messages:

Protocol Messages
=================
The Suunto Ambit sports watches communicate with the outside world via
messages in a tit-for-tat kind of fashion.  Application software sends
a request message to the device which then sends a response message in
return.  Message types abound and their sizes vary but every message
consists of a twelve byte header and a message type specific payload.
Any multi-byte *integers* in message headers as well as payloads are
encoded in `little endian`_ order.

.. _structure:

Structure
---------
The header is made up of a two byte message code, a byte of flags and
status info each, two byte format and counter fields and a four byte
payload size.  The payload itself is of the given size and follows
immediately after the header.

Lower-level protocol requirements may result in messages getting split
up into :ref:`packets <protocol-packets>`.

Stating this a bit more succinctly in `ABNF`_, that gives

.. productionlist::
   message: message-header message-payload
   message-header: `code` `flags` `status` `format` `counter` `payloadsize`
   message-payload: *OCTET

Schematically, a message looks as illustrated below.

.. image:: message.svg

While the whole message is of course of interest, the remainder of
this section focuses on the red outlined parts which constitute the
message header.

Code
----
.. productionlist::
   code: 2OCTET

USB packet captures of `Moveslink2`_ communicating with various Ambit
watches have yielded a large number of message codes.  Quite a few of
these are now well understood.  For many others there is an educated
guess.  Some, however, still remain (a bit of) a mystery.

Message codes are two byte, non-integer entities and found at offset 0
of each message.  The codes below have been observed in USB packet
captures.  Insofar these codes have been reverse engineered, they are
documented.

.. note:: The documentation effort is a work in progress.  As long as
   this is here, there are (partially) "understood-but-undocumented"
   parts for the various codes.

.. object:: 0x0000

   Get :ref:`Handshake <handshake>` [request]

.. object:: 0x0002

   Get :ref:`Handshake <handshake>` [response]

.. object:: 0x0102

.. object:: 0x0103

.. object:: 0x0200

.. object:: 0x0202

.. object:: 0x0300

   Set :ref:`Date and Time <date-time>`

.. object:: 0x0302

   Set :ref:`Date <date-time>`

.. object:: 0x0306

   Get :ref:`Device Status <device-status>`

.. object:: 0x0700

.. object:: 0x0b00

.. object:: 0x0b01

.. object:: 0x0b02

.. object:: 0x0b03

.. object:: 0x0b04

.. object:: 0x0b05

.. object:: 0x0b06

.. object:: 0x0b07

.. object:: 0x0b08

.. object:: 0x0b0a

.. object:: 0x0b0b

.. object:: 0x0b15

.. object:: 0x0b16

.. object:: 0x0b17

.. object:: 0x0b18

.. object:: 0x0b19

.. object:: 0x0b1a

.. object:: 0x0b1b

.. object:: 0x0b1e

.. object:: 0x0b21

.. object:: 0x0b25

.. object:: 0x0b26

.. object:: 0x0b27

.. object:: 0x0e00

.. object:: 0x0e01

.. object:: 0x1100

.. object:: 0x1101

.. object:: 0x1104

.. object:: 0x1200

.. object:: 0x1201

.. object:: 0x1202

Flags
-----
.. productionlist::
   flags: OCTET

.. object:: 0x01

.. object:: 0x02

.. object:: 0x04

.. object:: 0x08

.. object:: 0x10


Status
------
.. productionlist::
   status: OCTET

.. object:: 0x00

.. object:: 0x01

.. object:: 0x02


Format
------
.. productionlist::
   format: 2OCTET

.. object:: 0x0000

.. object:: 0x0900

Counter
-------
.. productionlist::
   counter: %d0-%d65535

Payload Size
------------
.. productionlist::
   payloadsize: %d0-%d4294967295

A four byte, little endian integer, this value corresponds to the
number of bytes that follows the :ref:`message-header <structure>`.

.. _ABNF: https://en.wikipedia.org/wiki/Augmented_Backus–Naur_Form
.. _Moveslink2: http://www.movescount.com/connect/moveslink
.. _little endian: https://en.wikipedia.org/wiki/Endianness#Little-endian
