#! /usr/bin/env python3
#  conf.py -- Sphinx build configuration file
#  Copyright  © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>
#
#  SPDX-License-Identifier: CC-BY-SA-4.0

import sys
import os

project   = u'ambitious/read'
copyright = u'2019, Olaf Meeuwissen'

source_suffix  = '.rst'
master_doc     = 'index'

pygments_style = 'sphinx'
html_theme     = 'default'
html_style     = '/default.css'

primary_domain = None
default_role   = None
language       = None

extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
]

exclude_patterns = [
    '.git/',
    'build/',
]
