.. payloads.rst -- contained in protocol messages
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


.. _message-payloads:

Message Payloads
================

.. toctree::
   :maxdepth: 1

   payloads/handshake
   payloads/date-time
   payloads/device-status
