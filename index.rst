.. index.rst -- reverse engineered ambit documentation
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


Reverse Engineered Ambit Documentation
======================================

Communication Protocol
----------------------

.. warning:: This is *not* a specification!  This is just an attempt
   to document what has been discovered about the communication
   protocol for the Suunto Ambit family of sports watches.  As new
   firmware versions are released and new watches hit the market this
   information will eventually need to be updated.

The bulk of the Ambit communication protocol was initially reverse
engineered by Emil Ljungdahl.  Based on his findings, he wrote a C
library implementing the protocol and a `Wireshark`_ dissector to aid
further reverse engineering.  Emil announced the availability of his
`code on SourceForge`_ via the `Movescount`_ `Linux User Group`_ on
2013-11-04 and later moved his `Openambit`_ project to GitHub.

This documentation is based on his code for both the library and the
dissector, combined with ongoing analysis of old and new USB packet
captures.  It structures things slightly differently from the way
Emil's code does.  This is done in the hope that the new structure
yields a clearer picture.

.. toctree::
   :maxdepth: 1

   packets
   messages
   payloads

.. _Wireshark: https://www.wireshark.org/
.. _code on SourceForge: https://sourceforge.net/projects/openambit/
.. _MovesCount: http://www.movescount.com/
.. _Linux User Group: http://www.movescount.com/groups/group5135-Linux_User_Group
.. _Openambit: https://github.com/openambitproject/openambit

.. todolist::
