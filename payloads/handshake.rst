.. messages/handshake.rst -- establishing common ground
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


.. _handshake:

Handshake
=========

Request
-------

.. productionlist::
   handshake-request-payload: komposti-version
   komposti-version: `version`

Response
--------

.. productionlist::
   handshake-response-payload: nickname serialno version-info 4OCTET
   nickname: 16OCTET
   serialno: 16OCTET
   version-info: firmware-version hardware-version bootstrap-loader-version
   firmware-version: `version`
   hardware-version: `version`
   bootstrap-loader-version: `version`

Version
-------

.. productionlist::
   version: major minor patch
   major: %d0-%d255
   minor: %d0-%d255
   patch: %d0-%d65535
