.. messages/date-time.rst -- sync your watch with reality
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


.. _date-time:

Date And Time
=============

Request
-------

.. productionlist::
   set-date-time-payload: date time
   date: year month day
   time: hour minutes milliseconds
   year: %d0-%d65535
   month: %d0-%d255
   day: %d0-%255
   hour: %d0-%d255
   minutes: %d0-%d255
   milliseconds: %d0-%d65535

Date
----

.. productionlist::
   set-date-payload: date 4OCTET
