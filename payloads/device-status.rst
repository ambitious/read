.. messages/device-status.rst -- get some info about your watch
   Copyright © 2019  Olaf Meeuwissen <paddy-hack@member.fsf.org>

   SPDX-License-Identifier: CC-BY-SA-4.0


.. _device-status:

Device Status
=============

Response
--------

.. productionlist::
   device-status-response-payload: OCTET charge 2OCTET
   charge: %d0-%d100
